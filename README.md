# NetRadius Direct Auth Reactive Java Client

The Direct Auth Reactive Java Client is a slimmed down client that implements the necessary 
functionality an API server needs to use to authenticate and authorize clients. It does not
aim to implement access to the entire NetRadius IAM API. This client uses the HttpClient 
from Project Reactor underneath to provide asynchronous non-blocking I/O to the API.

## Downloads

Releases of this library are published  on [Maven Central](https://search.maven.org/).

 * Release Repository - https://repo.maven.apache.org/maven2/com/netradius/iam/directauth-reactive-client/
 * Staging Repository - https://oss.sonatype.org/content/groups/staging/com/netradius/iam/directauth-reactive-client/
 * Snapshot Repository - https://oss.sonatype.org/content/repositories/snapshots/com/netradius/iam/directauth-reactive-client/

## Java Compatibility

This project tests on Java LTS releases only. At present this library targets the Java 8 runtime and
is tested to work on Java 11.

## Bugs & Enhancement Requests

Please file any bugs or enhancements at https://bitbucket.org/netradius/iam-directauth-reactive-java-client/issues

## License
This project is licensed under the BSD 3-Clause License. See 
[LICENSE.txt](https://bitbucket.org/netradius/iam-directauth-java-client-reactor/src/master/LICENSE.txt)

## Local Development

To install a new jar file locally without running unit tests and skipping the GPG signing 
process, execute the following:

```bash
 ./mvnw -Dgpg.skip=true -DskipTests=true install
```
