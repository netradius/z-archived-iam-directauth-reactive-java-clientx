package com.netradius.iam.directauth.client.reactive;

import reactor.netty.http.client.HttpClient;
import reactor.netty.resources.ConnectionProvider;

public class DefaultHttpClientProvider implements DirectAuthHttpClientProvider {

  private ConnectionProvider connectionProvider;

  public DefaultHttpClientProvider() {}

  public DefaultHttpClientProvider(ConnectionProvider connectionProvider) {
    this.connectionProvider = connectionProvider;
  }

  @Override
  public HttpClient provide() {
    return connectionProvider == null ? HttpClient.create() : HttpClient.create(connectionProvider);
  }

}
