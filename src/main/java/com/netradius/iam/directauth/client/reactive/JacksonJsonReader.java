package com.netradius.iam.directauth.client.reactive;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import java.io.IOException;
import java.io.InputStream;

public class JacksonJsonReader implements JsonReader {

  private ObjectMapper objectMapper;

  public JacksonJsonReader() {
    this.objectMapper = new ObjectMapper();
    this.objectMapper.registerModule(new JavaTimeModule());
  }

  public JacksonJsonReader(ObjectMapper objectMapper) {
    this.objectMapper = objectMapper;
  }

  @Override
  public <T> T read(InputStream in, Class<T> clazz) throws IOException {
    return objectMapper.readValue(in, clazz);
  }
}
