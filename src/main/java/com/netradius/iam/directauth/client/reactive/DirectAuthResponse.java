package com.netradius.iam.directauth.client.reactive;

import java.util.UUID;

public class DirectAuthResponse {

  private UUID account;
  private UUID application;

  public DirectAuthResponse() {}

  public DirectAuthResponse(UUID account, UUID application) {
    this.account = account;
    this.application = application;
  }

  public UUID getAccount() {
    return account;
  }

  public void setAccount(UUID account) {
    this.account = account;
  }

  public UUID getApplication() {
    return application;
  }

  public void setApplication(UUID application) {
    this.application = application;
  }
}
