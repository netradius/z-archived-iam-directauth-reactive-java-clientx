package com.netradius.iam.directauth.client.reactive;

import com.netradius.authlib.direct.DirectAuthSecret;
import com.netradius.iam.directauth.client.reactive.exception.DirectAuthErrorException;
import com.netradius.iam.directauth.client.reactive.exception.DirectAuthException;
import com.netradius.iam.directauth.client.reactive.exception.DirectAuthUnauthorizedException;
import io.netty.handler.codec.http.HttpResponseStatus;
import reactor.core.publisher.Mono;
import reactor.netty.http.client.HttpClient;
import reactor.netty.http.client.HttpClientResponse;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;

public final class DirectAuthClient {

  public static final String AUDIENCE = "nr/iam";
  public static final String BASE_URL = "https://iam.netradius.net";
  public static final String DIRECT_AUTH_PATH = "/authorize/direct";

  private JsonReader jsonReader;
  private HttpClient httpClient;
  private DirectAuthSecret secret;
  private String baseUrl;

  private DirectAuthClient(JsonReader jsonReader, HttpClient httpClient, DirectAuthSecret secret, String baseUrl) {
    this.jsonReader = jsonReader;
    this.httpClient = httpClient;
    this.secret = secret;
    this.baseUrl = baseUrl;
  }

  private InputStream emptyStream() {
    return new ByteArrayInputStream(new byte[0]);
  }

  private UUID toUUID(String str) {
    try {
      return UUID.fromString(str);
    } catch (IllegalArgumentException | NullPointerException x) {
      throw new DirectAuthErrorException(String.format("Unable to parse UUID from %s", str));
    }
  }

  private DirectAuthErrorResponse toErrorResponse(InputStream in) {
    try {
      return jsonReader.read(in, DirectAuthErrorResponse.class);
    } catch (IOException x) {
      throw new DirectAuthErrorException(String.format("Unable to parse JSON response: %s", x.getMessage()), x);
    }
  }

  public Mono<DirectAuthResponse> authorize(String clientAuthorizationHeader, String audience, String path, String action) {
    return httpClient
        .baseUrl(baseUrl)
        .headers(b -> b
            .add("authorization-client", clientAuthorizationHeader)
            .add("authorization", secret.authorization(AUDIENCE, DIRECT_AUTH_PATH).getHeaderValue())
            .add("authorization-audience", audience)
            .add("authorization-path", path)
            .add("authorization-action", action))
        .post()
        .uri(DIRECT_AUTH_PATH)
        .responseSingle(((response, body) -> Mono.zip(Mono.just(response), body.asInputStream().defaultIfEmpty(emptyStream()))))
        .handle((t, s) -> {
          HttpClientResponse response = t.getT1();
          try {
            if (response.status() == HttpResponseStatus.OK) {
              UUID account = toUUID(response.responseHeaders().get("account"));
              UUID application = toUUID(response.responseHeaders().get("application"));
              s.next(new DirectAuthResponse(account, application));
            } else {
              String message = "Authorization failed";
              if (!response.responseHeaders().get("content-length").equals("0")) {
                DirectAuthErrorResponse errorResponse = toErrorResponse(t.getT2());
                message = errorResponse.getMessage();
              }
              if (response.status() == HttpResponseStatus.UNAUTHORIZED) {
                s.error(new DirectAuthUnauthorizedException(message));
              } else {
                s.error(new DirectAuthErrorException(message));
              }
            }
          } catch (DirectAuthException x) {
            s.error(x);
          }
        });
  }

  public static final class Builder  {

    private DirectAuthSecret secret;
    private DirectAuthSecretProvider secretProvider;
    private DirectAuthHttpClientProvider httpClientProvider;
    private String baseUrl = BASE_URL;
    private JsonReader jsonReader;

    public Builder secret(DirectAuthSecret secret) {
      this.secret = secret;
      return this;
    }

    public Builder secretProvider(DirectAuthSecretProvider secretProvider) {
      this.secretProvider = secretProvider;
      return this;
    }

    public Builder httpClientProvider(DirectAuthHttpClientProvider httpClientProvider) {
      this.httpClientProvider = httpClientProvider;
      return this;
    }

    public Builder jsonReader(JsonReader jsonReader) {
      this.jsonReader = jsonReader;
      return this;
    }

    public Builder baseUrl(String baseUrl) {
      this.baseUrl = baseUrl;
      return this;
    }

    private DirectAuthSecret resolveSecret() {
      if (secret != null) {
        return secret;
      }
      if (secretProvider != null) {
        return secretProvider.provide();
      }
      return new DefaultDirectAuthSecretProvider().provide();
    }

    private HttpClient resolveHttpClient() {
      if (httpClientProvider != null) {
        return httpClientProvider.provide();
      }
      return new DefaultHttpClientProvider().provide();
    }

    private JsonReader resolveJsonReader() {
      return jsonReader != null ? jsonReader : new JacksonJsonReader();
    }

    public DirectAuthClient build() {
      return new DirectAuthClient(resolveJsonReader(), resolveHttpClient(), resolveSecret(), baseUrl);
    }
  }

  static Builder builder() {
    return new Builder();
  }
}
