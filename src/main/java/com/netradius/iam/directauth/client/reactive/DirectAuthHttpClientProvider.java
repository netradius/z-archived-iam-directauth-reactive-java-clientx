package com.netradius.iam.directauth.client.reactive;

import reactor.netty.http.client.HttpClient;

public interface DirectAuthHttpClientProvider {

  HttpClient provide();

}
