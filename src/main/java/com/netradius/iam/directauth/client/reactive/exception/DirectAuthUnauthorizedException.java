package com.netradius.iam.directauth.client.reactive.exception;

public class DirectAuthUnauthorizedException extends DirectAuthException {

  public DirectAuthUnauthorizedException(String message) {
    super(message);
  }

}
