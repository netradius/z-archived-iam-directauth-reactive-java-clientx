package com.netradius.iam.directauth.client.reactive;

import java.io.IOException;
import java.io.InputStream;

public interface JsonReader {

  <T> T read(InputStream in, Class<T> clazz) throws IOException;

}
