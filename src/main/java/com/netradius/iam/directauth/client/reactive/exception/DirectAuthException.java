package com.netradius.iam.directauth.client.reactive.exception;

public abstract class DirectAuthException extends RuntimeException {

  private static final long serialVersionUID = -6967116704002034305L;

  public DirectAuthException() {}

  public DirectAuthException(String message) {
    super(message);
  }

  public DirectAuthException(String message, Throwable t) {
    super(message, t);
  }

  public DirectAuthException(Throwable t) {
    super(t);
  }

}
