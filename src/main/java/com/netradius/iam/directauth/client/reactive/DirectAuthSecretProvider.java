package com.netradius.iam.directauth.client.reactive;

import com.netradius.authlib.direct.DirectAuthSecret;

public interface DirectAuthSecretProvider {

  DirectAuthSecret provide();

}
