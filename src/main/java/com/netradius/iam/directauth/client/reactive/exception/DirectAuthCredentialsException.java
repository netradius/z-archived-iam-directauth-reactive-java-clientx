package com.netradius.iam.directauth.client.reactive.exception;

public class DirectAuthCredentialsException extends DirectAuthException {

  public DirectAuthCredentialsException() {
    super("Unable to locate secret for authentication");
  }

}
