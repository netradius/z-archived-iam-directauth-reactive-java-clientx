package com.netradius.iam.directauth.client.reactive.exception;

public class DirectAuthErrorException extends DirectAuthException {

  public DirectAuthErrorException(String message) {
    super(message);
  }

  public DirectAuthErrorException(String message, Throwable t) {
    super(message, t);
  }

}
