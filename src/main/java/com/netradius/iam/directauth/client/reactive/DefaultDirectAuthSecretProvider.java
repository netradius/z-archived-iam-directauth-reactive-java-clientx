package com.netradius.iam.directauth.client.reactive;

import com.netradius.authlib.direct.DirectAuthSecret;
import com.netradius.commons.lang.StringHelper;
import com.netradius.iam.directauth.client.reactive.exception.DirectAuthCredentialsException;

public class DefaultDirectAuthSecretProvider implements DirectAuthSecretProvider {

  public static final String SECRET_ENVIRONMENT_NAME = "NR_SDK_SECRET";
  public static final String SECRET_PROPERTY_NAME = "nr.sdk.secret";

  public DefaultDirectAuthSecretProvider() {}

  protected DirectAuthSecret resolveEnvironment() {
    String value = System.getenv(SECRET_ENVIRONMENT_NAME);
    return StringHelper.isNotBlank(value) ? DirectAuthSecret.builder().serialized(value).build() : null;
  }

  protected DirectAuthSecret resolveProperty() {
    String value = System.getProperty(SECRET_PROPERTY_NAME);
    return StringHelper.isNotBlank(value) ? DirectAuthSecret.builder().serialized(value).build() : null;
  }

  @Override
  public DirectAuthSecret provide() {
    DirectAuthSecret directAuthSecret = resolveEnvironment();
    if (directAuthSecret == null) {
      directAuthSecret = resolveProperty();
    }
    if (directAuthSecret == null) {
      throw new DirectAuthCredentialsException();
    }
    return directAuthSecret;
  }

}
