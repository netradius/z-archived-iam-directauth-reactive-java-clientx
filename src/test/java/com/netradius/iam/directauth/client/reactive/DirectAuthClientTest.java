package com.netradius.iam.directauth.client.reactive;

import com.netradius.authlib.direct.DirectAuthSecret;
import com.netradius.authlib.direct.DirectAuthType;
import com.netradius.iam.directauth.client.reactive.exception.DirectAuthUnauthorizedException;
import org.junit.Test;

import java.util.UUID;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;

public class DirectAuthClientTest {

  private static final String BASE_URL = "http://localhost:8080";
  private static final String HELLO_SERVER_SECRET = "S0pdHNsMnxCzlQJkVTUODFqSC+JPvJuMImAPz4E1rZPZt5aLDe3rZIEs2WZk3CY+111WBX+Ej990BpMi1asuvg==";
  private static final UUID HELLO_SERVER_KEY_ID = UUID.fromString("5cb5acc7-6fb7-409c-be17-319fb7cb9568");
  private static final UUID HELLO_CLIENT_ACCOUNT_ID = UUID.fromString("44d307f0-18db-442a-a5ec-d722bf09f9a5");
  private static final UUID HELLO_CLIENT_APPLICATION_ID = UUID.fromString("2fb06e04-47f3-4296-9595-1c33ff5e5790");
  private static final String HELLO_CLIENT_SECRET = "frUkGTiG6xm/7dGb5wmStuMIbZiYgPvVdYZTu2E8SkCAdq91ou2RMItA+L/iL2XilVttd7yzC9yQZSWzM4Ancg==";
  private static final UUID HELLO_CLIENT_KEY_ID = UUID.fromString("c15c50fb-e089-4cb9-a418-5c0221dfced7");
  private static final String HELLO_AUDIENCE = "nr/hello";
  private static final String HELLO_PATH = "/";
  private static final String HELLO_ACTION = "Hello";

  private DirectAuthSecret serverSecret;
  private DirectAuthSecret clientSecret;

  private DirectAuthSecret serverSecret() {
    if (serverSecret == null) {
      this.serverSecret = DirectAuthSecret.builder()
          .type(DirectAuthType.V1_HMAC_SHA512)
          .keyId(HELLO_SERVER_KEY_ID)
          .secretKey(HELLO_SERVER_SECRET)
          .build();
    }
    return serverSecret;
  }

  private DirectAuthSecret badServersSecret() {
    return DirectAuthSecret.generate(DirectAuthType.V1_HMAC_SHA512);
  }

  private DirectAuthSecret clientSecret() {
    if (clientSecret == null) {
      this.clientSecret = DirectAuthSecret.builder()
          .type(DirectAuthType.V1_HMAC_SHA512)
          .keyId(HELLO_CLIENT_KEY_ID)
          .secretKey(HELLO_CLIENT_SECRET)
          .build();
    }
    return clientSecret;
  }

  private String clientAuthorization() {
    return clientSecret()
        .authorization(HELLO_AUDIENCE, HELLO_PATH)
        .getHeaderValue();
  }

  private DirectAuthClient client() {
    return DirectAuthClient.builder()
        .secret(serverSecret())
        .baseUrl(BASE_URL)
        .build();
  }

  @Test
  public void testHappyPath() {
    DirectAuthClient client = client();
    String clientAuthorization = clientAuthorization();
    DirectAuthResponse response = client.authorize(clientAuthorization, HELLO_AUDIENCE, HELLO_PATH, HELLO_ACTION).block();
    assertThat(response, notNullValue());
    assertThat(response.getAccount(), equalTo(HELLO_CLIENT_ACCOUNT_ID));
    assertThat(response.getApplication(), equalTo(HELLO_CLIENT_APPLICATION_ID));
  }

  @Test(expected = DirectAuthUnauthorizedException.class)
  public void testBadSecret() {
    DirectAuthClient client = DirectAuthClient.builder().secret(badServersSecret()).baseUrl(BASE_URL).build();
    String clientAuthorization = clientAuthorization();
    client.authorize(clientAuthorization, HELLO_AUDIENCE, HELLO_PATH, HELLO_ACTION).block();
  }
}
